package com.devcamp.rectanglerestapi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.rectanglerestapi.Rectangle;

@RestController
public class RectangleController {
@GetMapping("/rectangle-area")
    public double getRectangleArea(@RequestParam(value = "length", defaultValue = "1.0") float length,
            @RequestParam(value = "width", defaultValue = "1.0") float width) {
        Rectangle rectangle = new Rectangle(length, width);
        return rectangle.getArea();
    }

    @GetMapping("/rectangle-perimeter")
    public double getRectanglePerimeter(@RequestParam(value = "length", defaultValue = "1.0") float length,
            @RequestParam(value = "width", defaultValue = "1.0") float width) {
        Rectangle rectangle = new Rectangle(length, width);
        return rectangle.getPerimeter();
    }
}
